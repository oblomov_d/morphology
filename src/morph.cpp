#include <cmath>
#include <cstdio>
#include <cstdlib>

#include <opencv2/opencv.hpp>

#define uint8 unsigned char

uint8 mini(uint8 a, uint8 b){
    return a > b ? b : a;
}

uint8 maxi(uint8 a, uint8 b){
    return a > b ? a : b;
}

int clampi(int val, int min, int max){
    if (val < min)
        return min;
    else if (val > max)
        return max;
    else return val;
}

uint8 padded(cv::Mat &img, int y, int x, uint8 pad_value = 255){
   if (y < 0 || y > img.rows || x < 0 || x > img.cols)
       return pad_value;
   return img.at<uint8>(y, x);
}

uint8 clamped(cv::Mat &img, int y, int x){
    int i = clampi(y, 0, img.rows);
    int j = clampi(x, 0, img.cols);
    return img.at<uint8>(i, j);
}

void calcHmaps(cv::Mat &img, cv::Mat &head, cv::Mat &tail,
        int wx, uint8 (*func)(uint8, uint8)){
    int m = img.rows;
    int n = img.cols;
    for (int i = 0; i < m; i++){
        head.at<uint8>(i, 0) = img.at<uint8>(i, 0);
        tail.at<uint8>(i, n-1) = img.at<uint8>(i, n-1);
        for (int j = 1, jb = n-2; j < n; j++, jb--){
            head.at<uint8>(i, j) = 
                ((j-wx-1) % (wx+wx+1)) ? func(head.at<uint8>(i, j-1), img.at<uint8>(i, j)) : img.at<uint8>(i, j);
            tail.at<uint8>(i, jb) = 
                ((jb-wx-1+1)%(wx+wx+1)) ? func(tail.at<uint8>(i, jb+1), img.at<uint8>(i, jb)) : img.at<uint8>(i, jb);
        }
    }
}

void calcVmaps(cv::Mat &img, cv::Mat &head, cv::Mat &tail,
        int wy, uint8 (*func)(uint8, uint8)){
    int m = img.rows;
    int n = img.cols;
    for (int j = 0; j < n; j++){
        head.at<uint8>(0, j) = img.at<uint8>(0, j);
        tail.at<uint8>(m-1, j) = img.at<uint8>(m-1, j);
        for (int i = 1, ib = m-2; i < m; i++, ib--){
            head.at<uint8>(i, j) = 
                ((i-wy-1) % (wy+wy+1)) ? func(head.at<uint8>(i-1, j), img.at<uint8>(i, j)) : img.at<uint8>(i, j);
            tail.at<uint8>(ib, j) = 
                ((ib-wy-1+1)%(wy+wy+1)) ? func(tail.at<uint8>(ib+1, j), img.at<uint8>(ib, j)) : img.at<uint8>(ib, j);
        }
    }
}

void procH(cv::Mat &head, cv::Mat &tail, 
        cv::Mat &eroded, int wx, uint8 (*func)(uint8, uint8)){
    int m = head.rows;
    int n = head.cols;
    for (int j = 0; j < n; j++){
        int jl = clampi(j-wx, 0, n);
        int jr = clampi(j+wx, 0, n);
        for (int i = 0; i < m; i++){
            eroded.at<uint8>(i, j) = func(tail.at<uint8>(i, jl), head.at<uint8>(i, jr));
        }
    }
}

void procV(cv::Mat &head, cv::Mat &tail, 
        cv::Mat &eroded, int wy, uint8 (*func)(uint8, uint8)){
    int m = head.rows;
    int n = head.cols;
    for (int i = 0; i < m; i++){
        int it = clampi(i-wy, 0, m);
        int ib = clampi(i+wy, 0, m);
        for (int j = 0; j < n; j++){
            eroded.at<uint8>(i, j) = func(tail.at<uint8>(it, j), head.at<uint8>(ib, j));
        }
    }
}

void erode1(cv::Mat &img, cv::Mat &res, int wx, int wy){
    cv::Mat head(img.rows, img.cols, CV_8UC1, cv::Scalar(0));
    cv::Mat tail(img.rows, img.cols, CV_8UC1, cv::Scalar(0));
    calcHmaps(img, head, tail, wx, &mini);
    procH(head, tail, res, wx, &mini);
    calcVmaps(res, head, tail, wy, &mini);
    procV(head, tail, res, wy, &mini);
}

void dilate1(cv::Mat &img, cv::Mat &res, int wx, int wy){
    cv::Mat head(img.rows, img.cols, CV_8UC1, cv::Scalar(0));
    cv::Mat tail(img.rows, img.cols, CV_8UC1, cv::Scalar(0));
    calcHmaps(img, head, tail, wx, &maxi);
    procH(head, tail, res, wx, &maxi);
    calcVmaps(res, head, tail, wy, &maxi);
    procV(head, tail, res, wy, &maxi);
}

void open(cv::Mat &img, int wx, int wy){
    cv::Mat eroded(img.rows, img.cols, CV_8UC1, cv::Scalar(0));
    cv::Mat dilated(img.rows, img.cols, CV_8UC1, cv::Scalar(0));

    erode1(img, eroded, wx, wy);
    dilate1(eroded, dilated, wx, wy);

    cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE);
    cv::imshow("Display Image", dilated);
    cv::imwrite("open.png", dilated);
    cv::waitKey(0);
}

void close(cv::Mat &img, int wx, int wy){
    cv::Mat eroded(img.rows, img.cols, CV_8UC1, cv::Scalar(0));
    cv::Mat dilated(img.rows, img.cols, CV_8UC1, cv::Scalar(0));

    dilate1(img, dilated, wx, wy);
    erode1(dilated, eroded, wx, wy);

    cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE);
    cv::imshow("Display Image", eroded);
    cv::imwrite("close.png", dilated);
    cv::waitKey(0);
}

void test(cv::Mat &img, int wx, int wy){
    open(img, wx, wy);
    close(img, wx, wy);
}

int main(int argc, char **argv){
    if (argc != 4){
        printf("usage: %s <image_path>\n", argv[0]);
        return -1;
    }

    cv::Mat img;
    img = cv::imread(argv[1], cv::IMREAD_GRAYSCALE);
    int wx, wy;
    wx = std::atoi(argv[2]);
    wy = std::atoi(argv[3]);

    if (!img.data){
        printf("No img data \n");
        return -1;
    }
    test(img, wx, wy);

    //cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE);
    //cv::imshow("Display Image", img);
    //cv::waitKey(0);

    return 0;
}

